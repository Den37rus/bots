from database import Database
import asyncio
from telethon import TelegramClient, client, events
from telethon.sessions.string import StringSession
from colorama import Fore, Back, init as color_ama
color_ama(autoreset=True)


#       TO DO
#     Create module for user


class Handlers:
    def __init__(self):
        pass

    async def visit_handler(self, event):
        if 'You will be redirected' in event.raw_text:
            site_url = await self.click_on_btn(event, 'Go to ')
            print(Fore.CYAN + site_url + Fore.RESET)

    async def chats_handler(self, event):
        print('Chats')

    async def money_earned_handler(self, event):
        if 'for visiting a site!' in event.raw_text:
            print(
                f'{Fore.MAGENTA}{event.raw_text[4:].capitalize()}{Fore.RESET}')


class Actions:
    async def click_on_btn(self, event, text_btn):
        buttons = await event.get_buttons()
        for bline in buttons:
            for button in bline:
                if text_btn in button.button.text:
                    await button.click()
                    return button.url if button.url else button.button.text

    async def start_visit(self):
        print(
            f'{Back.WHITE}{Fore.RED}            Start sites visit              {Back.RESET}{Fore.RESET}\n')
        self.client.add_event_handler(self.visit_handler, events.NewMessage(
            chats=self.url_channel, incoming=True))
        await self.client.send_message(self.url_channel, '🖥 Visit sites')
        await asyncio.sleep(self.SITES_TIMEOUT)
        self.client.remove_event_handler(self.visit_handler, events.NewMessage(
            chats=self.url_channel, incoming=True))
        return

    async def start_chats(self):
        print(
            f'{Back.LIGHTBLUE_EX}{Fore.WHITE}            Start join chats           {Back.RESET}{Fore.RESET}\n')
        self.client.add_event_handler(self.chats_handler, events.NewMessage(
            chats=self.url_channel, incoming=True))
        await self.client.send_message(self.url_channel, '📣 Join chats')
        # Timeout to relsolve handler lock in case of unexpected stops
        await asyncio.sleep(self.CHATS_TIMEOUT)
        self.client.remove_event_handler(self.chats_handler, events.NewMessage(
            chats=self.url_channel, incoming=True))
        return


class Client(Database, Handlers, Actions):

    def __init__(self):
        super().__init__()
        self.client = TelegramClient(
            StringSession(self.user['session']), self.user['api_id'], self.user['api_hash'])

    async def connect(self):
        try:
            if self.client:
                await self.client.connect()
            if not await self.client.is_user_authorized():
                #  TO DO    sending unauthorized alert
                print('auth failed')
            else:
                print(
                    f'{Fore.GREEN}Account #{self.user["id"]} {Fore.CYAN}{self.user["phone"]}{Fore.RESET}')

                self.client.add_event_handler(self.money_earned_handler, events.NewMessage(
                    chats=self.url_channel, incoming=True))

        except Exception as e:
            print(e)

    async def disconnect(self):
        self.update_db_session(StringSession.save(self.client.session))
        self.next_account()
        await self.client.disconnect()


class Bot(Client):

    def __init__(self):
        super().__init__()

    async def run(self):
        while True:
            try:
                await self.connect()
                if self.client.is_connected():
                    # await self.start_visit()
                    await self.start_chats()
                    # await self.start_visit()
                    await self.disconnect()
            except Exception as bot_exception:
                print(bot_exception)


print('=================================================================================================================')

if __name__ == '__main__':
    v1 = Bot()
    # v1.run()
    loop = asyncio.get_event_loop()
    tasks = asyncio.wait([loop.create_task(v1.run())])
    loop.run_until_complete(tasks)
