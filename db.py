
import time
import os
import shutil
import sqlite3
from colorama import Back, Fore
import colorama
colorama.init()


accs_db_filename = 'Account.db'

db = sqlite3.connect(accs_db_filename)
cur = db.cursor()
cur.execute("""CREATE TABLE IF NOT EXISTS Account (
    ID INTEGER PRIMARY KEY,
    PHONE TEXT,
    PASS TEXT,
    API_ID TEXT,
    API_HASH TEXT,
    LITECOIN TEXT
              )""")
db.commit()
db.close()

dbalance = sqlite3.connect('Balance.db')
curDb = dbalance.cursor()

curDb.execute("""CREATE TABLE IF NOT EXISTS Balance (
    NUMBER_ROW INTEGER PRIMARY KEY,
    ID INTEGER,
    PHONE TEXT,
    CHECK_DATE INTEGER,
    BALANCE TEXT
    )""")
dbalance.commit()
dbalance.close()


def new_balance_record(id, phone, balance_value):
    try:
        dbalance = sqlite3.connect('Balance.db')
        curDb = dbalance.cursor()

        res = curDb.execute(
            f"SELECT * FROM Balance WHERE PHONE={phone}").fetchall()
        to_remove_old_recs = res[0:-11]
        if len(to_remove_old_recs):
            for item in to_remove_old_recs:
                curDb.execute(
                    f"DELETE from Balance where NUMBER_ROW={item[0]}")
                dbalance.commit()

        current_date = int(time.time()*1000)
        curDb.execute("""INSERT INTO Balance (ID,PHONE, CHECK_DATE, BALANCE) VALUES (?,?,?,?)""",
                      ([id, str(phone), current_date, str(balance_value)]))
        dbalance.commit()
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if dbalance:
            dbalance.close()


def get_all_last_balance():
    try:
        accounts_db = sqlite3.connect(accs_db_filename)
        accs_cursor = accounts_db.cursor()
        accounts = accs_cursor.execute(f"SELECT PHONE FROM Account").fetchall()
        accounts_count = len(accounts)
        accounts_db.close()

        last_balance = []

        dbalance = sqlite3.connect('Balance.db')
        curDb = dbalance.cursor()
        for ac in accounts:
            res = curDb.execute(
                f"SELECT ID,PHONE,CHECK_DATE,BALANCE FROM Balance WHERE PHONE = '{ac[0]}' ORDER BY NUMBER_ROW DESC").fetchone()
            last_balance.append(res)
        return last_balance
    except sqlite3.Error as sqlErr:
        print(sqlErr)
    except Exception as ex:
        print(ex)
    finally:
        if dbalance:
            dbalance.close()


def get_all_balance():
    try:
        accounts_db = sqlite3.connect(accs_db_filename)
        accs_cursor = accounts_db.cursor()
        accounts = accs_cursor.execute(f"SELECT PHONE FROM Account").fetchall()
        accounts_count = len(accounts)
        accounts_db.close()

        last_balance = []

        dbalance = sqlite3.connect('Balance.db')
        curDb = dbalance.cursor()
        for acc in accounts:
            res = curDb.execute(
                f"SELECT ID,PHONE,CHECK_DATE,BALANCE FROM Balance WHERE PHONE = '{acc[0]}'").fetchall()
            last_balance.append(res)
        return last_balance
    except sqlite3.Error as sqlErr:
        print(sqlErr)
    except Exception as ex:
        print(ex)
    finally:
        if dbalance:
            dbalance.close()


def __createAccount(accoun_data):
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()

        if len(accoun_data) != 5:
            print(Fore.RED + 'Account data must be list of 5 elements' + Fore.RESET)
        else:
            cur.execute(
                f"SELECT PHONE FROM Account WHERE PHONE='{accoun_data[0]}'")
            if cur.fetchone() is None:
                cur.execute(
                    """INSERT INTO Account (PHONE, PASS, API_ID, API_HASH, LITECOIN) VALUES (?,?,?,?,?)""", (accoun_data))
                db.commit()
                print(Fore.BLACK + Back.LIGHTGREEN_EX + ' Account for phone: ' +
                      accoun_data[0] + ' created ' + Fore.RESET + Back.RESET)
            else:
                print(Fore.LIGHTRED_EX +
                      ' !!!   DataBase allredy exist this phone number   !!! ' + Fore.RESET)
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def createAccObject():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        new_phone = input('Input phone number: ')
        if not cur.execute(f"SELECT PHONE FROM Account WHERE PHONE='{new_phone}'").fetchone() is None:
            print(
                Fore.RED + '!!!   DataBase allredy exist this phone number   !!!' + Fore.RESET)
            return None
        new_pass = input('Input password: ')
        new_api_id = input('Input API_ID: ')
        new_api_hash = input('Input API_HASH: ')
        new_litecoin = input('Input litecoin adress: ')
        return [new_phone, new_pass,
                new_api_id, new_api_hash, new_litecoin]
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __accounReplace():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        needID = input('№ акаунта который заменить: ')
        newData = input('№ акаунта из которго заменить: ')

        old = cur.execute("SELECT * FROM Account WHERE ID = ?",
                          [str(newData)]).fetchone()
        cur.execute(
            f"UPDATE Account SET PHONE = '{old[1]}', PASS='{old[2]}', API_ID='{old[3]}', API_HASH='{old[4]}', LITECOIN='{old[5]}' WHERE ID='{needID}'")
        db.commit()
        to_file = os.path.join(os.path.abspath(
            os.path.dirname(__file__)), f'session/anon{needID}.session')
        from_file = os.path.join(os.path.abspath(
            os.path.dirname(__file__)), f'session/anon{newData}.session')
        shutil.copy2(from_file, to_file)
        result = cur.execute(
            f"SELECT * FROM Account WHERE ID = '{needID}'").fetchone()
        print(Fore.CYAN + '========= Account changed ============' + Fore.RESET)
        print(Fore.CYAN + 'New data for acc № ' +
              needID + ' - ' + Fore.RESET + str(result))
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __editAccount():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        print(f'Database contains {databaseLength()} records')
        index = input('Account ID for edit: ')
        if int(index) > databaseLength():
            print('No inputed ID in base')
            return
        acc_data = cur.execute("SELECT * FROM Account WHERE ID = ?",
                               [str(index)]).fetchone()
        print(acc_data)
        phone = input('Enter phone number: ')
        pwd = input('Enter password: ')
        api_id = input('Enter API id: ')
        api_hash = input('Enter API hash: ')
        if not phone == '':
            cur.execute(
                f"UPDATE Account SET PHONE='{phone}' WHERE ID='{index}'")
        if not pwd == '':
            cur.execute(f"UPDATE Account SET PASS='{pwd}' WHERE ID='{index}'")
        if not api_id == '':
            cur.execute(
                f"UPDATE Account SET API_ID='{api_id}' WHERE ID='{index}'")
        if not api_hash == '':
            cur.execute(
                f"UPDATE Account SET API_HASH='{api_hash}' WHERE ID='{index}'")
        db.commit()
        new_data = cur.execute(
            "SELECT * FROM Account WHERE ID = ?", [str(index)]).fetchone()
        print(
            f'Updated= > ID: {new_data[0]}, Phone: {new_data[1]}, Password: {new_data[2]}, API_ID: {new_data[3]}, API_HASH: {new_data[4]}')
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def printAllAccounts():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        db_list = cur.execute("SELECT * from Account").fetchall()
        for item in db_list:
            print(str(item))
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def databaseLength():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        return len(cur.execute("SELECT * FROM Account").fetchall())
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def currentAccountData(idx):
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        user = cur.execute(
            f"SELECT * FROM Account WHERE ID='{idx}'").fetchone()
        return user
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __deleteAccount__(idx):
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        if databaseLength() == 0:
            print("Database is empty")
            return
        if int(idx) > databaseLength():
            print(Fore.RED + "Out of range" + Fore.RESET)
        cur.execute(
            f"DELETE FROM Account WHERE ID={idx}")
        db.commit()
        cur.execute(f"UPDATE Account SET ID = ID-1 WHERE ID > {idx}")
        db.commit()
        try:
            path = os.path.join(os.path.abspath(
                os.path.dirname(__file__)), 'session/anon' + idx + '.session')
            os.remove(path)
            chats = os.path.join(os.path.abspath(
                os.path.dirname(__file__)), 'bots_chats_data/chats' + idx + '.txt')
            for ind in range(int(idx), databaseLength()+1):
                print(ind)
                os.rename(f'session/anon{ind + 1}.session',
                          f'session/anon{ind}.session')
                os.rename(f'bots_chats_data/chats{ind + 1}.txt',
                          f'bots_chats_data/chats{ind}.txt')
        except FileNotFoundError:
            print("File with ID: " + idx + " not found")
            return
        # while True:
        print(databaseLength()+1)

        print(Fore.RED + "Deleted acc - " + idx + Fore.RESET)
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __extractAccountsToFile(fileName):
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        file_name = fileName or 'BotsAccounts'
        currentBotId = open(f'{file_name}.txt', 'w')
        res = db.execute("SELECT * FROM Account").fetchall()
        for i in res:
            currentBotId.write(str(i) + '\n')

        print(Fore.GREEN + f'File {file_name}.txt writen' + Fore.RESET)

    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __coin_change():
    try:
        db = sqlite3.connect(accs_db_filename)
        cur = db.cursor()
        with open('coins.txt', 'r') as coins_file:
            litecoin_array = list(filter(lambda x: len(
                x) > 1, coins_file.read().split('\n')))
        db_id = 1
        res = db.execute("SELECT * FROM Account")
        while db_id < databaseLength()+1:
            for i in range(0, len(litecoin_array)):
                if db_id > databaseLength()+1:
                    break
                db.execute(
                    f"UPDATE Account SET 'LITECOIN' = '{litecoin_array[i]}' WHERE id={db_id}")
                db.commit()
                db_id += 1
    except sqlite3.Error as error:
        print("Ошибка при работе с SQLite", error)
    finally:
        if db:
            db.close()


def __fastCreateAccount(acc=None):
    if acc:
        account_string = acc.split(',')
        account_string.append('')
        __createAccount(account_string)
        return
    account_string = input('Input Account data: ').split(',')
    account_string.append('')
    __createAccount(account_string)


if __name__ == "__main__":
    while True:
        action = input(
            'Change action: \n    1-create\n    2-delete\n    3-replace\n    4-DB to file\n    5-ltc coins update\n    6-edit one\n    7-create from list\n    8-create from file\n    Enter for watch all accounts\n    0 for Exit\n')
        if action == '1':
            new_account = createAccObject()
            if new_account == None:
                continue
            __createAccount(new_account)
        elif action == '2':
            acc_id = input('Account number: ')
            __deleteAccount__(acc_id)
        elif action == '3':
            __accounReplace()
        elif action == '4':
            name = input('Input filename: ')
            if not name:
                name = None
            __extractAccountsToFile(name)
        elif action == '5':
            __coin_change()
        elif action == '6':
            __editAccount()
        elif action == '7':
            __fastCreateAccount()
        elif action == '8':
            file_name = input('Input filename: ')
            try:
                with open(file_name, 'r', encoding='utf-8') as accs_file:
                    accs = accs_file.read().split('\n')
                    for acc in accs:
                        acc.split(',')
                        __fastCreateAccount(acc=acc)
            except FileNotFoundError:
                print('Bad filename')
        elif action == '0':
            break
        else:
            printAllAccounts()
