import psycopg2
from psycopg2 import extras, ProgrammingError


def exact(sql, values=[]):
    try:
        conn = psycopg2.connect(dbname='bot', user='python',
                                password='py', host='localhost')
        cur = conn.cursor(cursor_factory=extras.DictCursor)

        cur.execute(sql, values)
        conn.commit()
        return cur.fetchone()
    except ProgrammingError:
        pass
    except Exception as global_exception:
        print(global_exception)
    finally:
        conn.commit()
        conn.close()


def exact_all(sql, values=[]):
    try:
        conn = psycopg2.connect(dbname='bot', user='python',
                                password='py', host='localhost')
        cur = conn.cursor(cursor_factory=extras.DictCursor)

        cur.execute(sql, values)
        conn.commit()
        return cur.fetchall()
    except ProgrammingError:
        pass
    except Exception as global_exception:
        print(global_exception)
    finally:
        print('close connection')
        conn.commit()
        conn.close()


class Database:
    def __init__(self):
        exact('''create table if not exists users (
                    id serial primary key,
                    phone varchar(15) not null,
                    password varchar(60) not null,
                    api_id varchar(60) not null,
                    api_hash varchar(60) not null,
                    litecoin varchar(60),
                    activity boolean default FALSE,
                    session text
            )''')

        exact('''create table if not exists balance (
                    id serial primary key,
                    amount numeric default 0.0000,
                    rec_date TIMESTAMP default NOW(),
                    acc_id INTEGER REFERENCES users (id)
            )''')

    url_channel = 'Litecoin_click_bot'
    db_length = exact('select count(*) from users')[0]
    current_id = exact('select id from users where activity = True')['id']
    user = exact('select * from users where id = %s', [current_id])

    SITES_TIMEOUT = 60
    CHATS_TIMEOUT = 10

    def update_db_session(self, new_session_string):
        exact('update users set session = %s where id = %s',
              [new_session_string, self.user['id']])

    def next_account(self):
        exact('update users set activity = False where id = %s',
              [self.user['id']])
        exact('update users set activity = True where id = %s',
              [self.user['id'] + 1] if self.current_id <= self.db_length else 1)


if __name__ == '__main__':
    db = Database()

    res = db.current_account_data(db.get_current_account_id())
    print(res)
