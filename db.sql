create table if not exists users (
    id serial primary key,
    phone varchar(15) not null,
    password varchar(60) not null,
    api_id varchar(60) not null,
    api_hash varchar(60) not null,
    litecoin varchar(60),
    activity boolean default FALSE,
    session text
);
create table if not exists balance (
    id serial primary key,
    amount numeric default 0.0000,
    rec_date TIMESTAMP default NOW(),
    acc_id INTEGER REFERENCES users (id)
);
select array(
        select amount,
            rec_date
        from balance
        where acc_id = 1
    );